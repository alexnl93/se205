#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include "circular_buffer.h"
#include "protected_buffer.h"

protected_buffer_t * protected_buffer_init(int length) {
  protected_buffer_t * b;
  b = (protected_buffer_t *)malloc(sizeof(protected_buffer_t));
  b->buffer = circular_buffer_init(length);

  //création d'une var cond pour proteger contre les acces conccurents en lecture
  //Deux verrous
  // si il y a écritutre : ni lecture ni ecriture
  // si lecture : pas d'autre lecture possible
  
  sem_init(&(b->sem_writer),0,4); //4 ressouces en ecriture à t = 0  
  sem_init(&(b->sem_reader),0,0); //0 ressouces en lecture à t = 0
  
  return b;
}

void * protected_buffer_get(protected_buffer_t * b){
  void * d;
   
  sem_wait(&(b->sem_reader)); //on peut lire si il y a plus de donnés que de lecteurs dans le sémaphore
  pthread_mutex_lock(&(b->mutex_buffer)); //acces un par un au tampon circulaire
  
  d = circular_buffer_get(b->buffer);

  pthread_mutex_unlock(&(b->mutex_buffer)); //on libère l'action au tampon

  sem_post(&(b->sem_writer)); //on libère une ressource en ecriture
  
  return d;
}

int protected_buffer_put(protected_buffer_t * b, void * d){  
  
  sem_wait(&(b->sem_writer)); //on peut ecrire si il y a plus de place que d'écrivains dans le sémaphore
  pthread_mutex_lock(&(b->mutex_buffer)); //acces un par un au tampon circulaire

  circular_buffer_put(b->buffer, d);

  pthread_mutex_unlock(&(b->mutex_buffer)); //on libère l'action au tampon
  sem_post(&(b->sem_reader)); //on libère une ressource en lecture
  
  return 1;
}
   
