#ifndef PROTECTED_BUFFER_H
#define PROTECTED_BUFFER_H
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include "circular_buffer.h"

typedef struct {
  circular_buffer_t * buffer;
  sem_t sem_writer;
  sem_t sem_reader;
  pthread_mutex_t mutex_buffer;
  
} protected_buffer_t;

// Initialise la structure du tampon circulaire
protected_buffer_t * protected_buffer_init(int length);

// Retire une donnée du tampon circulaire et bloque s'il est vide.
void * protected_buffer_get(protected_buffer_t * b);

// Ajoute une donnée dans le tampon circulaire et bloque s'il est plein
int protected_buffer_put(protected_buffer_t * b, void * d);
#endif
