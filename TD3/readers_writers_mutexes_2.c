#include "readers_writers_mutexes_2.h"

const char READER_K = 'R';
const char WRITER_K = 'W';
const char UNUSED_K = 'U';

void * READER;
void * WRITER;
void * UNUSED;

void rw_mutex_init (rw_mutex_t * rw_mutex, int n_threads){
  READER = (void *) &READER_K;
  WRITER = (void *) &WRITER_K;
  UNUSED = (void *) &UNUSED_K;
  rw_mutex->n_readers = 0;
  rw_mutex->owner = UNUSED;

  //me
  rw_mutex->queue = circular_buffer_init(n_threads);
  pthread_cond_init(&(rw_mutex->guard), NULL);
}

void rw_mutex_read_lock (rw_mutex_t *rw_mutex, thread_conf_t *conf){
  int i;

  pthread_mutex_lock(&(rw_mutex->mutex));
  circular_buffer_put(rw_mutex->queue, READER);
  while(rw_mutex->owner != UNUSED) //à modifier
    pthread_cond_wait(&(rw_mutex->guard), &(rw_mutex->mutex));
  
  rw_mutex->n_readers = rw_mutex->n_readers + 1;
  rw_mutex->owner = READER;
  circular_buffer_get(rw_mutex->queue);
 
  if(circular_buffer_read(rw_mutex->queue) == READER)
    pthread_cond_signal(&(rw_mutex->guard));
 
  pthread_mutex_unlock(&(rw_mutex->mutex));
  
  for (i=0; i < rw_mutex->n_readers; i++) printf ("  ");
  printf ("reader (%ld) : enter r = %d v = %ld\n",
	  conf->identifier, rw_mutex->n_readers, shared_variable);
}

void rw_mutex_read_unlock (rw_mutex_t *rw_mutex, thread_conf_t * conf){
  int i;
  
  pthread_mutex_lock(&(rw_mutex->mutex));

  rw_mutex->n_readers = rw_mutex->n_readers - 1;
  
  if(rw_mutex-> n_readers == 0)
    {
      
      rw_mutex->owner = UNUSED;
      if(circular_buffer_read(rw_mutex->queue) == WRITER){
	pthread_cond_signal(&(rw_mutex-> guard));
      }
    }

  pthread_mutex_unlock(&(rw_mutex->mutex));
 
  for (i=0; i < rw_mutex->n_readers; i++) printf ("  ");
  printf ("reader (%ld) : leave r = %d v = %ld\n",
	  conf->identifier, rw_mutex->n_readers, shared_variable);
}

void rw_mutex_writer_lock (rw_mutex_t *rw_mutex, thread_conf_t * conf){

  pthread_mutex_lock(&(rw_mutex->mutex));
  circular_buffer_put(rw_mutex->queue, WRITER);
  while(rw_mutex->owner  != UNUSED)
    pthread_cond_wait(&(rw_mutex->guard), &(rw_mutex->mutex));
  
  rw_mutex->owner = WRITER;
  circular_buffer_get(rw_mutex->queue);

  pthread_mutex_unlock(&(rw_mutex->mutex));
}

void rw_mutex_writer_unlock (rw_mutex_t *rw_mutex, thread_conf_t * conf){
  pthread_mutex_lock(&(rw_mutex->mutex));
  rw_mutex->owner = UNUSED;
  pthread_cond_signal(&(rw_mutex->guard));
  pthread_mutex_unlock(&(rw_mutex->mutex));
}
